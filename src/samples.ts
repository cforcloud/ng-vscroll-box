// TODO more dynamic
/* tslint:disable:max-line-length */

const IMAGE_1 = '<div><div><img class="w-100p" alt="hey" src="https://images.unsplash.com/photo-1511424443513-a00662140eeb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80"/></div><a class="credit" target="_blank" rel="noopener" href="https://unsplash.com/photos/69wg9vWwPAU">unsplash@sunyu</a></div>';
const IMAGE_2 = '<div class="align-right"><div><img class="w-100p" alt="hey" src="https://images.unsplash.com/photo-1551225183-94acb7d595b6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=650&q=80"/></div><a class="credit" target="_blank" rel="noopener" href="https://unsplash.com/photos/o1xcUi-Yt_w">unsplash@afafa</a></div>';
const IMAGE_3 = '<div class="align-center"><div><img class="w-100p" alt="hey" src="https://images.unsplash.com/photo-1518110516893-31ce851decb0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"/></div><a class="credit" target="_blank" rel="noopener" href="https://unsplash.com/photos/Rf77y7Q_WLk">unsplash@chriswormhoudt</a></div>';
const IMAGE_4 = '<div class="align-center"><div><img class="w-100p" alt="hey" src="https://images.unsplash.com/photo-1560015534-cee980ba7e13?ixlib=rb-1.2.1&auto=format&fit=crop&w=200&q=80"/></div><a class="credit" target="_blank" rel="noopener" href="https://unsplash.com/photos/kgqu_qs3B78">unsplash@gozlukluf</a></div>';


const loreums: Array<string> = [
  /// plain text
  'Maiores dolore perspiciatis fuga quae.',
  'Placeat nihil est quas similique cupiditate asperiores enim. Vitae nobis quis animi.',
  'Id atque veniam dolorem beatae in. Provident ad tenetur sed sapiente est. Maiores dolorem est illum fugit cumque labore doloremque. Placeat nihil est quas Vitae nobis quis animi.',
  'Repellat alias aut impedit eveniet atque maxime. Delectus qui praesentium vel labore. Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae. Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae.',
  'Earum eveniet consequatur autem nobis et voluptatem. Libero qui suscipit veniam id ut consequatur. Aut amet pariatur sit porro amet necessitatibus iste qui. Facere quod natus consequatur qui iste ad. Eum perferendis eaque occaecati culpa vitae. Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae.',
];

const htmlLoreums: Array<string> = [
  /// plain html
  'Hi Friend!!',
  'Hello Universe!!',

  /// Sanitizer will remove `javascript:` and <style>
  '<p>Maiores dolore <strong onclick="javascript:alert(123)">perspiciatis</strong> <style>p { color: red; }</style> fuga quae.</p>',

  '<p>Placeat nihil est quas similique cupiditate asperiores enim. Vitae nobis quis animi.</p>',
  '<p>Id atque veniam dolorem beatae in. <em>Provident ad tenetur sed sapiente est.</em> Maiores dolorem est illum fugit cumque labore doloremque.</p><p>Placeat nihil est quas Vitae nobis quis animi.</p>',
  '<p>Repellat alias aut impedit eveniet atque maxime. Delectus qui praesentium vel labore. Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae.</p><p>Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae.</p>',
  '<p>Earum eveniet consequatur autem nobis et voluptatem. Libero qui suscipit veniam id ut consequatur. Aut amet pariatur sit porro amet necessitatibus iste qui. Facere quod natus consequatur qui iste ad.</p><p>Eum perferendis eaque occaecati culpa vitae.</p><p>Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae.</p>',
];

const richLoreums: Array<string> = [
  ...htmlLoreums,
  /// with image
  `<p>IMAGE_1 Libero qui suscipit veniam id ut consequatur. </p> ${IMAGE_1} <p>Fugiat atque voluptatum magni temporibusad tenetur sed sapiente est.</p>`,
  `<p>IMAGE_2 Delectus qui praesentium vel temporibus ullam aut labore. Libero qui suscipit veniam id ut consequatur.</p> ${IMAGE_2} <p>Fugiat atque voluptatum magni temporibusad tenetur sed sapiente est.</p>`,
  `<p>IMAGE_3 Veniam libero qui suscipit id ut consequatur.</p> ${IMAGE_3} <p>Fugiat quod natus consequatur qui iste atque voluptatum magni temporibusad tenetur sed sapiente est. Delectus qui praesentium vel temporibus ullam aut labore.</p>`,
  `<p>IMAGE_4 Consequatur libero qui suscipit veniam id ut consequatur.</p> ${IMAGE_4} <p>Fugiat atque voluptatum magni temporibusad tenetur sed sapiente est. Fugiat atque voluptatum magni temporibus ullam aut minus.</p>`,

  /// with video
  `<span>Wisdom full!</span><div><span>↓</span> <a target="_blank" href="https://youtu.be/4fYUTpq0Ous">https://youtu.be/4fYUTpq0Ous</a><br>
 <iframe width="400" height="220" src="https://www.youtube-nocookie.com/embed/4fYUTpq0Ous" frameborder="0" allow="encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>`,

  /// with gif
  `<span>Subject : Hey <br>Hi <span><a href='#'>@Black Panther</a></span>,<br><br>Good Morning<br><br>
 <div class='gifplayer-wrapper' style='width: 135px; height: 135px;'><img class='ghiphy_responsive gifPlayer 28882 gifplayer-ready' alt='ghiphy' src='https://media0.giphy.com/media/xM8CKTlDAN5Xa/giphy.gif?cid=e1bb72ff5c9c3bd3393277496bbbe058&time=0.8778578278247877' width='135' height='135'></div>`,

];

function getRandNum(min: number = 0, max: number = 800): number {
  return min + Math.floor(Math.random() * (max - min));
}

function getRandText(arr: Array<string>): string {
  return arr[getRandNum(0, arr.length)];
}

export function getItem(id: number): any {
  const content = `Item # ${id}: ${getRandText(htmlLoreums)}`;
  const hasButtons = (Math.random() < 0.5);
  // const content2 = (Math.random() < 0.2) ? getRandText() : null;
  return {
    id,
    content,
    hasButtons,
    // token: id,
    // content2,
  };
}

export function getItems(startId: number, arrayLength: number, idOffset: number = 0): Array<any> {
  const items = Array(arrayLength).fill(0).map((_, index) => getItem(startId + index + idOffset));

  // console.log('getItems', { startId, arrayLength, items });
  return items;
}


/*

const loreum: String = `
Maiores dolore perspiciatis fuga quae. Placeat nihil est quas similique cupiditate asperiores enim. Vitae nobis quis animi.
Id atque veniam dolorem beatae in. Provident ad tenetur sed sapiente est. Maiores dolorem est illum fugit cumque labore doloremque.
Autem nostrum non totam et quaerat. Est maiores commodi sapiente aspernatur. Rerum consectetur iusto voluptas voluptatem quidem sapiente impedit dignissimos. Itaque a beatae ea.
Repellat alias aut impedit eveniet atque maxime. Delectus qui praesentium vel labore. Fugiat atque voluptatum magni temporibus ullam aut minus. Eum perferendis eaque occaecati culpa vitae.
Libero qui suscipit veniam id ut consequatur. Earum eveniet consequatur autem nobis et voluptatem. Aut amet pariatur sit porro amet necessitatibus iste qui. Facere quod natus consequatur qui iste ad.
`;

*/

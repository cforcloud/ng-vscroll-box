
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { NgVscrollBoxModule } from 'ng-vscroll-box';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListItemComponent } from './list-item/list-item.component';
import { DemoSimpleComponent } from './demo-simple/demo-simple.component';
// import { DemoChatComponent } from './demo-chat/demo-chat.component';

@NgModule({
  declarations: [
    AppComponent,
    ListItemComponent,
    DemoSimpleComponent,
    // DemoChatComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgVscrollBoxModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

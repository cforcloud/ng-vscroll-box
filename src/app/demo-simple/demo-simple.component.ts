import { Component, OnInit, AfterViewInit, ViewChild, NgZone } from '@angular/core';

import { NgVscrollBoxComponent, ChangeEvent, UpdateEvent } from 'ng-vscroll-box';
import { getItems } from '../../samples';

@Component({
  selector: 'app-demo-simple',
  templateUrl: './demo-simple.component.html',
  // styleUrls: ['./demo-simple.component.scss']
})
export class DemoSimpleComponent implements OnInit, AfterViewInit {

  @ViewChild(NgVscrollBoxComponent, { static: false })
  scrollRef: NgVscrollBoxComponent;

  items: Array<any> = [];
  viewPortItems: Array<any> = [];
  idCount = 1;

  toId = 1100;
  toPosition = 10000;

  isAuto = false;
  isLoading = false;

  checkObj: any = {};
  checkStr = '';

  constructor(
    private ngZone: NgZone,
  ) { }

  ngOnInit() {}

  ngAfterViewInit(): void {
    requestAnimationFrame(() => {
      this.onAddItems('append', 102);
    });
  }

  onAddItems(type: string = 'append', count: number = 10) {
    const newItems = getItems(this.idCount, count);
    this.idCount += count;
    if (type === 'append') {
      this.items = [ ...this.items, ...newItems ];
    } else if (type === 'prepend') {
      this.items = [ ...newItems, ...this.items ];
    }

    // console.log('onAddItems', type, count);
    this.doCheck();
  }

  loadItems(updateEvent: UpdateEvent) {
    if (this.isAuto && !this.isLoading) {
      const { atTop, atBottom } = updateEvent;

      if (atTop || atBottom) {
        this.isLoading = true;

        setTimeout(() => {
          this.onAddItems(atTop ? 'prepend': 'append', 10);
          this.isLoading = false;
        }, 500);

        console.log('loadItems', { atTop, atBottom });
      }
    }
  }

  onChangeEvent(changeEvent: ChangeEvent) {
    this.viewPortItems = changeEvent.viewPortItems;
    this.doCheck(changeEvent);
    // console.log('onVscrollChange', changeEvent);
  }

  onUpdateEvent(updateEvent: UpdateEvent) {
    this.loadItems(updateEvent);
    this.doCheck(updateEvent);
    // console.log('onVscrollUpdate', updateEvent);
  }

  onReset() {
    this.items = [];
    this.isAuto = false;
    this.isLoading = false;
    this.doCheck();
  }

  onScrollTo(type: string = 'append') {
    switch (type) {
      case 'top':
        this.scrollRef.scrollToTop();
        break;
      case 'bottom':
        this.scrollRef.scrollToBottom();
        break;
      case 'id':
        this.scrollRef.scrollToId(this.toId);
        break;
      case 'position':
        this.scrollRef.scrollToPosition(this.toPosition);
        break;
      default:
    }

    // this.scrollRef.viewPortItems
    console.log('onScrollTo', type);
  }

  onInsertItemEvent(insertEvent: any) {
    const { id: itemId, count, isBelow } = insertEvent;

    const item = this.items.find(a => (a.id === itemId));
    const index = this.items.indexOf(item);
    const index1 = isBelow ? (index + 1) : index;

    const before = this.items.slice(0, index1);
    const after = this.items.slice(index1);

    const newItems = getItems(this.idCount, count);
    this.idCount += count;

    this.items = [...before, ...newItems, ...after];

    // console.log('onInsertItemEvent', { itemId, count, isBelow, item, index, index1, before, after, newItems });
  }

  onRemoveItemEvent(itemId: number) {
    this.items = this.items.filter(a => (a.id !== itemId));
    // console.log('onRemoveItemEvent', itemId);
  }

  onTodo() {
    console.log('TODO');
  }

  doCheck(obj: any = {}) {
    this.checkObj.itemsLength = this.items.length;
    this.checkObj.viewPortItemsLength = this.viewPortItems.length;
    this.checkObj = { ...this.checkObj, ...obj };
    // checkObj.isLoading = this.isLoading;
    delete this.checkObj.items;
    delete this.checkObj.viewPortItems;

    this.ngZone.run(() => {
      this.checkStr = JSON.stringify(this.checkObj, null, 1);
    });

    // console.log('doCheck', { obj, checkObj: this.checkObj });
  }

}

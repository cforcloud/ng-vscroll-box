import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { NgVscrollBoxModule } from 'ng-vscroll-box';
import { DemoSimpleComponent } from './demo-simple.component';
import { ListItemComponent } from '../list-item/list-item.component';

describe('DemoSimpleComponent', () => {
  let component: DemoSimpleComponent;
  let fixture: ComponentFixture<DemoSimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DemoSimpleComponent,
        ListItemComponent
      ],
      imports: [
        FormsModule,
        NgVscrollBoxModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

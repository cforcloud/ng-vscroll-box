import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoSimpleComponent } from './demo-simple/demo-simple.component';
// import { DemoChatComponent } from './demo-chat/demo-chat.component';

const routes: Routes = [
  { path: '', redirectTo: '/demo-simple', pathMatch: 'full' },
  { path: 'demo-simple', component: DemoSimpleComponent },
  // { path: 'demo-chat', component: DemoChatComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

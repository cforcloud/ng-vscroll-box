import {
  Component, OnInit,
  Input, Output, EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() item: any;
  @Input() index: number;

  @Output()
  public insertEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  public removeEvent: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    // console.log('ngOnInit', this.index);
  }

  onInsertEvent(count: number = 0, isBelow: boolean = true) {
    const { id } = this.item;
    this.insertEvent.emit({ id, count, isBelow });
  }

  onRemoveItem() {
    this.removeEvent.emit(this.item.id);
  }

  onTodo() {
    console.log('TODO', this.item.id);
  }

}

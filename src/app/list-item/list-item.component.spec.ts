import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemComponent } from './list-item.component';

/// refer dashboard-hero.component.spec.ts

describe('ListItemComponent', () => {
  let component: ListItemComponent;
  let fixture: ComponentFixture<ListItemComponent>;
  let debugEl: DebugElement;
  let htmlEl: HTMLElement;

  const index: number = 10;
  const item: any = { id: 20, content: 'Test <strong>Content</strong>' };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListItemComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemComponent);
    component = fixture.componentInstance;

    // find the DebugElement and element
    debugEl = fixture.debugElement.query(By.css('.list-item-wrapper'));
    htmlEl = debugEl.nativeElement;

    // simulate the parent setting the input property
    component.index = index;
    component.item = item;

    // trigger initial data binding
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display index', () => {
    const indexEl: HTMLElement = htmlEl.querySelector('.check-index');
    expect(indexEl.textContent).toContain(''+ index);
  });

  it('should display content', () => {
    const contentEl: HTMLElement = htmlEl.querySelector('.content');
    const expectedContent: string = item.content;
    expect(contentEl.innerHTML).toContain(expectedContent);
  });
});

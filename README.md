# ng-vscroll-box

Angular 8 virtual scroll for variable heights

[Demo and Docs](https://gitlab.com/cforcloud/ng-vscroll-box/tree/master/projects/ng-vscroll-box#ng-vscroll-box)


<details>
<summary>Quick start for development</summary>

```sh
# scaffolding
ng new ng-vscroll-box
cd ng-vscroll-box

# in angular.json replace 'ng-vscroll-box' with 'ng-vscroll-box-demo'

# generate library
ng generate library ng-vscroll-box

# in src/app/app.module.ts add NgVscrollBoxModule
import { NgVscrollBoxModule } from 'ng-vscroll-box';

# run the project
ng build ng-vscroll-box --watch
ng serve

# publish
ng build ng-vscroll-box
cd dist/ng-vscroll-box/
npm publish

# update angular
ng update
ng update @angular/cli @angular/core
```
</details>

<!--
<details>
<summary>Angular</summary>
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
</details>
-->

# v0.3.0
* Feature - `atTop` and `atBottom` in `updateEvent` using for auto loading
* Items can be dynamically inserted or deleted
* Measure cache validation and clean up after insertion or deletion
* Updated dependencies - `@angular/cli@8.3.4` `@angular/core@8.2.6`
* Minor Fix - new scrollTo in `toPosition`

# v0.2.3
* Angular 6 compatible - removed unused service, which causes error
* Minimum `@angular/core` version is `6.1.0`
* Respects hidden attribute
* Description change
* Minor Fix - height check in `toPosition`

# v0.2.2
* Minor Fix - Added `lodash.debounce` to `peerDependencies`

# v0.2.1
* Minor change in event names - use `changeEvent` and `updateEvent`
* Use `lodash.debounce` instead of `lodash`
* Readme docs update
* Added badges in Readme
* Temp fix SSR error
* Code lint
* Unit test covers 58% statements

# v0.2.0
* Feature - `idProp` to specify unique key in `items`
* Fixes scroll to top/bottom issues
* Additional docs in Readme

# v0.1.0
* Initial version
* Feature
    * Rich long text
    * Variable heights
    * Prepend and append items
    * Scroll to top or bottom
    * Scroll to item id

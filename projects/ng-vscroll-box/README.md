# ng-vscroll-box

[![npm](https://img.shields.io/npm/v/ng-vscroll-box?style=flat-square)](https://www.npmjs.com/package/ng-vscroll-box) [![angular](https://img.shields.io/badge/angular-8.x-red.svg?logo=angular&style=flat-square)](https://angular.io/) [![minzipped size](https://flat.badgen.net/bundlephobia/minzip/ng-vscroll-box)](https://bundlephobia.com/result?p=ng-vscroll-box@latest) [![license MIT](https://img.shields.io/npm/l/ng-vscroll-box?style=flat-square)](https://gitlab.com/cforcloud/ng-vscroll-box/blob/master/LICENSE)

`ng-vscroll-box` is a [Angular](https://angular.io/) virtual scroll for items with variable heights, especially chat like interfaces.

## Features
- Items with variable heights, by default
- Dynamic prepend or append items
- Dynamic insert or delete items
- Scroll to top or bottom
- Scroll to item
- Scroll to position
<!-- - Expand or collapse item (show more) -->
<!-- - Stick to bottom -->
<!-- - Custom scroll bar with styling -->

## [Live Demo with source](https://codesandbox.io/s/ng-vscroll-box-demo-5520e)
<!--
TODO
## Advanced Live demo with chat
-->

## Usage

**1.** Install with peer dependencies

```sh
npm i ng-vscroll-box resize-observer-polyfill lodash.debounce
```
**2.** Update your app module

```js
import { NgVscrollBoxModule } from 'ng-vscroll-box';
...

@NgModule({
  ...
  imports: [
    ...
    NgVscrollBoxModule,
    ],
  ...
})
export class AppModule { }
```

**3.** Use the `ng-vscroll-box` tag. Each item in items array must have unique key named `id`. This value is used internally for caching sizes.

```html
<ng-vscroll-box #scrollRef
  [items]="items"
  >
  <div
    *ngFor="let item of scrollRef.viewPortItems"
    [vscrollItemId]="item.id"
    >
    {{item.name}}
  </div>
</ng-vscroll-box>
```

**4.** Specify height and/or width using CSS

```css
ng-vscroll-box {
  height: 80vh;
  /* width: 500px; */
  /* border: 1px solid silver; */
}
```

## Peer Dependencies
<!-- > Apart from @angular/common and @angular/core, two modules are required -->

* `resize-observer-polyfill` 1.5.x - polyfill until all browsers support [ResizeObserver](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver)
* `lodash.debounce` 4.x.x - for [debounce](https://lodash.dev/docs/4.17.15#debounce) function


## Properties
- `[items]: Array<any>` required

   The array of objects passed to the virtual scroll. It's important that each array item is an object with a unique key named `id` or `idProp` described below.

- `[idProp]: string` optional, default value `id`

   Name of the unique key in every item in `items`. This will be used internally for tracking. The value of the unique key can be `number` or `string`.

- `(changeEvent): ChangeEvent`

   Emits `viewPortItems` which has to be looped to generate list components, `startIndex` and `endIndex`.

- `(updateEvent): UpdateEvent`

  Emits values `atTop`, `atBottom`, `scrollTop`, `scrollHeight`.

## Methods

- `scrollToTop()`

   Scrolls to top

- `scrollToBottom()`

   Scrolls to bottom

- `scrollToId(id: number | string)`

   Scrolls to item. Pass the `idProp` value of the item.

- `scrollToPosition(top: number)`

   Scrolls to pixel position


<details>
<summary>Values</summary>

```
   .-----------.        -            -                     -
   |           |        |            | contentTop*         |
   | .-------. |        | scrollTop  -  -                  |
   | |       | |        |               |                  |
 .-+-+-------+-+-.   -  -               |                  |
 | | |       | | |   |                  |                  |
 | | |       | | |   | height           | contentHeight*   | scrollHeight
 | | |       | | |   |                  |                  |
 `-+-+-------+-+-'   -                  |                  |
   | |       | |                        |                  |
   | `-------' |                        -                  |
   |           |                                           |
   |           |                                           |
   `-----------'                                           -

 * internal values
```
</details>

## Angular
This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.

### [Change log](https://gitlab.com/cforcloud/ng-vscroll-box/blob/master/CHANGELOG.md)

### [License MIT](https://gitlab.com/cforcloud/ng-vscroll-box/blob/master/LICENSE)

<!--
<details>
<summary>Angular</summary>

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## Code scaffolding

Run `ng generate component component-name --project ng-vscroll-box` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project ng-vscroll-box`.
> Note: Don't forget to add `--project ng-vscroll-box` or else it will be added to the default project in your `angular.json` file.

## Build

Run `ng build ng-vscroll-box` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build ng-vscroll-box`, go to the dist folder `cd dist/ng-vscroll-box` and run `npm publish`.

## Running unit tests

Run `ng test ng-vscroll-box` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

</details>
-->

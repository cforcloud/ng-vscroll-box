/*
 * Public API Surface of ng-vscroll-box
 */

export * from './lib/ng-vscroll-box.component';
export * from './lib/ng-vscroll-item.directive';
export * from './lib/ng-vscroll-box.module';

/// ng6 throws error, not used for now
// export * from './lib/ng-vscroll-box.service';

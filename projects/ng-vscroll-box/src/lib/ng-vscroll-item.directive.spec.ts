import { Component, DebugElement, Input, ViewChild } from '@angular/core';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgVscrollItemDirective } from './ng-vscroll-item.directive';

@Component({
  selector: 'app-test-component',
  template: `<div [vscrollItemId]="id">test content</div>`
})
class TestComponent {
  @Input() id: number | string;

  /// add a reference to the test element in template
  @ViewChild(NgVscrollItemDirective, { static: true })
  vscrollItemDirective: NgVscrollItemDirective;
}

describe('NgVscrollItemDirective', () => {

  let component: TestComponent;
  let directive: NgVscrollItemDirective;
  let fixture: ComponentFixture<TestComponent>;
  let debugEl: DebugElement;

  const id: number = 10;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent, NgVscrollItemDirective ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;

    /// find the DebugElement
    debugEl = fixture.debugElement.query(By.css('div'));

    directive = component.vscrollItemDirective;
    // directive = fixture.debugElement.query(By.directive(NgVscrollItemDirective));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have id and itemRef', () => {
    component.id = id;
    fixture.detectChanges();
    expect(directive.id).toEqual(id);
    expect(directive.itemRef).toBeTruthy(id);
    // console.log('directive', directive);
  });

  it('should throw if no unique id given on init', () => {
    // component.id = id;

    expect(function() {
      fixture.detectChanges();
    }).toThrowError(/needs a unique id/);
  });

});

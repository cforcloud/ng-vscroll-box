/// utils

// bind to keep line numbers in the console
export const log = true ? console.log.bind(console, 'vscroll~') : () => {};

export function getSize(element: HTMLElement): ClientRect {
  const result = element.getBoundingClientRect();
  // const styles = getComputedStyle(element);
  // const marginTop = parseInt(styles['margin-top'], 10) || 0;
  // const marginBottom = parseInt(styles['margin-bottom'], 10) || 0;
  // let marginLeft = parseInt(styles['margin-left'], 10) || 0;
  // let marginRight = parseInt(styles['margin-right'], 10) || 0;

  return {
    // top: result.top + marginTop,
    // bottom: result.bottom + marginBottom,
    // left: result.left + marginLeft,
    // right: result.right + marginRight,
    // width: result.width + marginLeft + marginRight,
    // height: result.height + marginTop + marginBottom
    top: 0, bottom: 0, left: 0, right: 0,
    width: result.width,
    height: result.height,
  };
}

/// is difference between num1 and num2 is more maxDiff
export function isDiff(num1: number, num2: number, maxDiff: number = 5) {
  return (Math.abs(num1 - num2) > maxDiff);
}

export function isNumber(value) {
  return (typeof value === 'number');
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgVscrollBoxComponent } from './ng-vscroll-box.component';
import { NgVscrollItemDirective } from './ng-vscroll-item.directive';

@NgModule({
  declarations: [
    NgVscrollBoxComponent,
    NgVscrollItemDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NgVscrollBoxComponent,
    NgVscrollItemDirective
  ]
})
export class NgVscrollBoxModule { }

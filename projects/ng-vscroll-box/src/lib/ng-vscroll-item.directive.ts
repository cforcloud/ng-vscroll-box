import { Directive, OnInit, ElementRef, Input } from '@angular/core';

let thrown = false;

@Directive({
  selector: '[vscrollItemId]'
})
export class NgVscrollItemDirective implements OnInit {

  @Input('vscrollItemId') id: number | string;

  constructor(public itemRef: ElementRef) {
    // console.log('NgVscrollItem', this.id, itemRef);
  }

  ngOnInit() {
    /// id should be valid number or string
    if (!thrown && !(this.id || this.id === 0)) {
      thrown = true;
      const tagName: string = this.itemRef.nativeElement.tagName.toLowerCase(); // || 'div'

      // console.log('NgVscrollItem', this.id, tagName, this.itemRef);
      throw new Error(`ng-vscroll-box: Element [vscrollItemId] needs a unique id to work properly. Use it like <${tagName} [vscrollItemId]='item.id' />`);
    }
  }

}

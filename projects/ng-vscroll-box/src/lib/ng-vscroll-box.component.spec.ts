import { Component, DebugElement, Input, ViewChild } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture, async, fakeAsync, tick, flush } from '@angular/core/testing';

import { NgVscrollBoxComponent } from './ng-vscroll-box.component';
import { NgVscrollItemDirective } from './ng-vscroll-item.directive';

@Component({
  selector: 'app-test-component',
  template: `<div class="my-4">
    <ng-vscroll-box #scrollRef
      [idProp]="idProp"
      [items]="items"
      >
      <div
        *ngFor="let item of scrollRef.viewPortItems;"
        [vscrollItemId]="item[idProp]"
        class="item"
        >
        {{item.content}}
      </div>
    </ng-vscroll-box>
  </div>`,
	styles: [`
    ng-vscroll-box {
      height: 300px;
      border: 1px solid silver;
    }
    .item {
      box-shadow: 0px 0px 0px 1px #EEE;
      padding: 8px;
    }
    .my-4 {
      margin-top: 1rem;
      margin-bottom: 1rem;
    }
  `],
})
class TestComponent {
  @Input() idProp: string = 'id';
  @Input() items: Array<any> = [];

  /// add a reference to the test element in template
  @ViewChild(NgVscrollBoxComponent, { static: true })
  scrollRef: NgVscrollBoxComponent;
}

const lorem: Array<string> = `Vitae ut cumque possimus sequi vitae laboriosam. Itaque qui rerum sequi. Vero voluptate hic quae id harum molestiae aut sequi. Quam et aut qui qui quia quis veritatis. Hic magnam possimus deleniti at nemo. Aut delectus eos aut est placeat eum dolorum.`.split(' ');

function getRandNum(min: number = 0, max: number = 10): number {
  return min + Math.floor(Math.random() * (max - min));
}

function getRandText(): string {
  const len: number = getRandNum(5, 100);
  const str: Array<string> = [];
  const loremLength: number = lorem.length;
  for (let i = 0; i < len; i += 1) {
    str.push(lorem[getRandNum(0, loremLength)])
  }
  return str.join(' ');
}

function getItem(id: number): any {
  const content = `Item # ${id}: ${getRandText()}`;
  return {
    // id,
    token: id,
    content,
  };
}

function getItems(startId: number, arrayLength: number): Array<any> {
  const items = Array(arrayLength).fill(0).map((_, index) => getItem(startId + index));
  return items;
}

describe('NgVscrollBoxComponent', () => {
  let testComponent: TestComponent;
  let component: NgVscrollBoxComponent;
  let fixture: ComponentFixture<TestComponent>;
  let debugEl: DebugElement;

  const idProp: string = 'token';
  const items: Array<any> = getItems(0, 100);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestComponent,
        NgVscrollBoxComponent,
        NgVscrollItemDirective
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    // fixture = TestBed.createComponent(NgVscrollBoxComponent);
    testComponent = fixture.componentInstance;

    /// find the DebugElement
    debugEl = fixture.debugElement.query(By.css('div'));

    component = testComponent.scrollRef;

    // fixture.detectChanges();

    // console.log('component', component);
    // items.forEach(a => console.log('a', a));
  });

  it('should create', () => {
    expect(testComponent).toBeTruthy();
  });

  // TODO seperate tests
  // doMeasure is still not called
  it('should create with items', fakeAsync(() => {
    testComponent.idProp = idProp;
    testComponent.items = items;

    fixture.detectChanges();


    tick(2000);
    flush();
    fixture.detectChanges();

    // setTimeout(() => { called = true; }, 1000);

    // tick(2000);
    // flush();
    // fixture.detectChanges();

    component.scrollToBottom();

    let called = false;
    setTimeout(() => { called = true; }, 2000);
    tick(2000);
    flush();
    expect(called).toBe(true);

    fixture.detectChanges();

    component.scrollToTop();
    tick(2000);
    flush();
    fixture.detectChanges();

    component.scrollToId(50);
    tick(2000);
    flush();
    fixture.detectChanges();

    component.scrollToPosition(1000);
    tick(2000);
    flush();
    fixture.detectChanges();

    expect(component.idProp).toEqual(idProp);
    expect(component.items).toEqual(items);
    expect(component.viewPortItems.length).toEqual(25); // === _bufferMax

    // console.log(component.viewPortItems);
  }));

  // it('should create with items', fakeAsync(() => {
  //   testComponent.items = items;
  //
  //   fixture.detectChanges();
  //
  //   tick();
  //   // tick(2000);
  //   // flush();
  //
  //   fixture.detectChanges();
  //
  //   expect(component.items).toEqual(items);
  //   // expect(component.items).toEqual(items);
  //
  // }));
});

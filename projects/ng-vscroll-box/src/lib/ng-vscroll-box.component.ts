import {
  Component,
  Inject,
  OnInit, AfterViewInit, OnDestroy,
  Input,
  Output, EventEmitter,
  ElementRef, ViewChild,
  ContentChildren, QueryList,
  PLATFORM_ID,
  // HostListener,
  // NgZone
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
// import { DOCUMENT } from '@angular/common';

import ResizeObserver from 'resize-observer-polyfill';
import debounce from 'lodash.debounce';

import { NgVscrollItemDirective } from './ng-vscroll-item.directive';
import { log, getSize, isNumber, isDiff } from './utils';

// TODO
// e2e
// auto test
// dev tools
// custom scroll bar <div class="scrollbar"><div class="scrollbar-thumb"></div></div>
// clean up this.measures once in a while, if some items got deleted
// WebWorker
// *vScrollFor ng-template

/// on viewPortItems change
export interface ChangeEvent {
  viewPortItems: Array<any>;
  startIndex: number;
  endIndex: number;
}

/// on any scroll updates
export interface UpdateEvent {
  scrollTop: number;
  scrollHeight: number;
  height: number;
  atTop: boolean;
  atBottom: boolean;
  // scrollPercentage?: number;
}

@Component({
  selector: 'ng-vscroll-box',
  template: `
  <div class="scroll-wrapper" #wrapperRef (scroll)="onScroll()">

    <div class="scroll-spacer" [ngStyle]="spacerStyle"></div>

    <div class="scroll-content" #contentRef [ngStyle]="contentStyle">
      <ng-content></ng-content>
    </div>

  </div>
  `,
  styleUrls: ['./ng-vscroll-box.component.scss']
})
export class NgVscrollBoxComponent implements OnInit, AfterViewInit, OnDestroy {

  /// Inputs
  private _items: Array<any> = [];
  /**
   * Array of items to virtualize
   */
  @Input()
  public get items(): Array<any> {
    return this._items;
  }
  public set items(value: Array<any>) {
    if (value === this._items) {
      return;
    }

    this._items = value || [];

    this.storeState();
    this.debouncedDoCheck('items');
    this.debouncedDoValidate();

    // const prevItems = this._items;
    // const diff: any = diffArr(this._items, prevItems);
  }

  private _idProp = 'id';
  /**
   * Key name in every item in items, whose value will be unique
   */
  @Input()
  public get idProp(): string {
    return this._idProp;
  }
  public set idProp(value: string) {
    if (!value) {
      throw new Error('ng-vscroll-box: [idProp] value cannot be empty or null');
    }

    // const newValue: string = value || this._idProp;
    if (value === this._idProp) {
      return;
    }

    this._idProp = value;
  }

  /// public methods
  /**
   * Scroll to Top
   */
  public scrollToTop(): void {
    this.toPosition(0);
  }

  /**
   * Scroll to Bottom
   */
  public scrollToBottom(): void {
    this.toPosition(0, true);
  }

  /**
   * Scroll to item by id
   */
  public scrollToId(id: number | string): void {
    this._scrollToId = id;
    this.restoreState();
  }

  /**
   * Scroll to Position
   */
  public scrollToPosition(top: number): void {
    this.toPosition(top);
  }

  private _viewPortItems: Array<any> = [];
  /**
   * Items visible in ViewPort (read-only property)
   */
  public get viewPortItems(): Array<any> {
    return this._viewPortItems;
  }

  /// Outputs
  @Output()
  public changeEvent: EventEmitter<ChangeEvent> = new EventEmitter<ChangeEvent>();

  @Output()
  public updateEvent: EventEmitter<UpdateEvent> = new EventEmitter<UpdateEvent>();

  ///
  constructor(
    @Inject(PLATFORM_ID) private _platform: Object
  ) { }

  ngOnInit() {
    // this.scrollBarWidth = getScrollBarWidth();
  }

  ngAfterViewInit(): void {
    /// Avoid SSR Error
    if (!isPlatformBrowser(this._platform)) { return; }

    this._destroyed = false;
    this.addListeners();
  }

  ngOnDestroy(): void {
    this._destroyed = true;
    this.removeListeners();
  }

  /// Refs
  @ViewChild('wrapperRef', { static: true })
  private wrapperRef: ElementRef;

  @ViewChild('contentRef', { static: true })
  private contentRef: ElementRef;

  @ContentChildren(NgVscrollItemDirective)
  private itemChildren !: QueryList<NgVscrollItemDirective>;

  /// Viewport Elements
  private get wrapperElement(): HTMLElement {
    return this.wrapperRef.nativeElement;
  }

  private get contentElement(): HTMLElement {
    return this.contentRef.nativeElement;
  }

  /// Internal getters/setters

  private _scrollTop = 0;
  private get scrollTop(): number {
    return this._scrollTop;
  }
  private set scrollTop(value: number) {
    if (value === this._scrollTop) {
      return;
    }
    this._scrollTop = value || 0;

    // this.updateScrollPercentage();
    this.emitUpdate('scrollTop');
  }


  private _contentTop = 0;
  private set contentTop(value: number) {
    if (value === this._contentTop) {
      return;
    }
    this._contentTop = value || 0;

    this.contentStyle = { transform: `translateY(${this._contentTop}px)` };
    // log('contentTop', { _contentTop: this._contentTop });
  }

  private _scrollHeight = 100;
  private set scrollHeight(value: number) {
    if (value === this._scrollHeight) {
      return;
    }
    this._scrollHeight = value || 0;
    this.spacerStyle = { transform: `scaleY(${this._scrollHeight})` };
  }

  contentStyle: any = { transform: 'translateY(0px)' };
  spacerStyle: any = { transform: 'scaleY(100)' };

  private _destroyed = true;

  private _bufferCount = 5;
  private _bufferCount2: number = this._bufferCount * 2;
  private _bufferMax: number = this._bufferCount * 5;

  private _contentObserver: any = null;
  private _wrapperObserver: any = null;
  private _animationRequest: number;
  private _timer: any;

  private _wrapperHeight = 0;
  private _contentWidth = 0;
  private _contentHeight = 0;
  private _scrollTopMax = 0;
  private _scrollTopMin = 0;
  private _bufferHeight = 0;

  private _averageHeight = 0;
  private _averageHeightCounter = 0;

  private _currentId: number | string = null;
  private _scrollToId: number | string = null;
  private _currentOffset = 0;

  private _startIndex = 0;
  // private _endIndex = 0;

  private _measures: any = {};

  private debouncedDoCheck = debounce(
    (from: string = 'debounce') => this.doCheck(from),
    100, { maxWait: 200 }
  );

  private debouncedDoMeasure = debounce(
    (from: string = 'debounce') => this.doMeasure(from),
    100, { maxWait: 500 }
  );

  private debouncedDoValidate = debounce(
    () => this.doValidate(),
    5000, { maxWait: 10000 }
  );

  /// internal functions
  private addListeners(): void {
    this.removeListeners();

    const { wrapperElement, contentElement } = this;
    if (wrapperElement) {
      this._wrapperObserver = new ResizeObserver(() => { // entries
        this.debouncedDoMeasure('resize');
      });
      this._wrapperObserver.observe(wrapperElement);

      this._contentObserver = new ResizeObserver(() => {
        this.debouncedDoMeasure('resize');
      });
      this._contentObserver.observe(contentElement);
    }

    this.itemChildren.changes.subscribe(() => {
      if (this._averageHeight) {
        this.doMeasure('itemChildren.changes');
      }
    });
  }

  private removeListeners(): void {
    cancelAnimationFrame(this._animationRequest);
    clearTimeout(this._timer);
    this._timer = undefined;

    if (this._wrapperObserver !== null) {
      this._wrapperObserver.disconnect();
      this._wrapperObserver = null;
    }

    if (this._contentObserver !== null) {
      this._contentObserver.disconnect();
      this._contentObserver = null;
    }
  }

  private nextFrame(fun = (() => {})): void {
    this._animationRequest = requestAnimationFrame(() => fun());
    // this.zone.runOutsideAngular(() => {});
  }

  private timeOut(fun = (() => {}), wait: number = 50): void {
    this._timer = setTimeout(function() {
      this._timer = undefined;
      fun();
    }, wait);
  }

  onScroll(): void {
    this.nextFrame(() => {

      const { scrollTop } = this.wrapperElement;
      this.scrollTop = scrollTop;

      this.debouncedDoCheck('onScroll');
      // log('onScroll', { scrollTop });
    });
  }

  private doCheck(from: string = ''): void {
    if (this._destroyed) { return; }

    /// on reset or on start
    if (!this.items.length || !this._averageHeight) {
      this.emitChange(0);
      return;
    }

    if (this._averageHeight) {
      /// onScroll or from items

      const { scrollTop } = this;

      let isOutOfRange: boolean = (from === 'items');

      /// crossed bottom edge
      if (scrollTop > this._scrollTopMax) {
        // log('doCheck:bottom:', from, { isOutOfRange, scrollTop, _scrollTopMax: this._scrollTopMax });
        isOutOfRange = true;
      }
      /// crossed top edge
      if (scrollTop < this._scrollTopMin) {
        isOutOfRange = true;
      }

      if (isOutOfRange) {
        if (scrollTop <= this._bufferHeight) {
          /// at top / nearly at top
          this.emitChange(0);
          return;
        }
        // if (scrollTop <= this._bufferHeight) {
        //   /// TODO at bottom / nearly at bottom
        //   this.emitChange(0);
        //   return;
        // }

        const tempScrollTop: number = scrollTop - this._bufferHeight;

        /// find newStartIndex
        const { length } = this.items;
        let newStartIndex = 0;
        for (let i = 0; i < length; i += this._bufferCount2) {
          const { [this._idProp]: id } = this.items[i];
          const measureItem = this._measures[id];
          if (measureItem) {
            if (measureItem.top >= tempScrollTop) {
              break;
            }
            newStartIndex = i;
          }
          // log(0, { i, id, tempScrollTop, measureItem });
        }

        // log('doCheck:isOutOfRange', from, { newStartIndex, scrollTop });
        this.emitChange(newStartIndex);
        this.debouncedDoMeasure(from);
      }
    }
  }

  private doMeasure(from: string = ''): void {
    if (this._destroyed) { return; }

    this._wrapperHeight = Math.round(getSize(this.wrapperElement).height);
    const { width: contentWidth, height: contentHeight } = getSize(this.contentElement);

    /// flush if width has changed a bit
    if (isDiff(contentWidth, this._contentWidth, 20) || !this._contentWidth) {
      this._contentWidth = contentWidth;
      this._measures = {};
    }
    this._contentHeight = Math.round(contentHeight);

    if (!this._viewPortItems.length || !this.items.length) {
      this._scrollTopMax = 0;
      this._scrollTopMin = 0;
      this._averageHeight = 0;
      this._averageHeightCounter = 0;
      this.contentTop = 0;
      this.scrollHeight = 0;
      this._measures = {};

      // log('doMeasure:empty', { from });
      this.emitUpdate('doMeasure:empty');
      return;
    }

    /// itemChildren
    this.itemChildren.forEach(item => {
      const { id, itemRef } = item;
      const { height } = getSize(itemRef.nativeElement);
      this._measures[id] = { height };
      // log('itemChildren', { id, height });
    });

    this.updateAverageHeight();

    /// scrollHeight - sum of all heights / (last item's top + last item's height)
    let scrollHeight = 0;
    this.items.forEach(a => {
      const { [this._idProp]: id } = a;
      const measureItem = this._measures[id];

      if (!measureItem) {
        this._measures[id] = {};
      }

      this._measures[id].top = scrollHeight; // Math.round(scrollHeight)

      /// if not in _measures, use averageHeight
      scrollHeight += (measureItem && measureItem.height) ? measureItem.height : this._averageHeight;
    });
    scrollHeight = Math.round(Math.max(scrollHeight, contentHeight));
    this.scrollHeight = scrollHeight;

    /// contentTop is _startIndex item's top
    let contentTop = 0;
    if (this._startIndex) {
      const { [this._idProp]: id } = this.items[this._startIndex];
      const measureItem = this._measures[id];
      if (measureItem) {
        contentTop = measureItem.top;
      }
    }
    contentTop = Math.round(contentTop);
    this.contentTop = contentTop;

    /// buffer
    // this._bufferHeight = this._averageHeight * this._bufferCount;
    this._bufferHeight = 0;
    this._scrollTopMax = Math.ceil(contentTop + this._contentHeight - this._wrapperHeight - this._bufferHeight) + 1;
    this._scrollTopMin = contentTop;

    // log('doMeasure:', from, {
    //   itemChildren: this.itemChildren.toArray(),
    //   _measures: this._measures,
    //   scrollHeight,
    //   contentTop,
    //   _scrollTopMax: this._scrollTopMax,
    //   _scrollTopMin: this._scrollTopMin,
    // });

    this.emitUpdate('doMeasure');
    this.debouncedDoCheck('doMeasure');

    if (isNumber(this._currentId)) {
      this.restoreState();
    }
  }

  /// check measures and do cleanup
  private doValidate(): void {
    if (this._destroyed) { return; }

    const measureKeys = Object.keys(this._measures);
    const itemsKeys = this.items.map(a => String(a[this._idProp]));

    const clearKeys = measureKeys.filter(a => !itemsKeys.includes(a));

    clearKeys.forEach(key => {
      delete this._measures[key];
    });

    // console.log('doValidate', { clearKeys, _measures: this._measures });
  }

  private updateAverageHeight(): void {
    /// first 10 times and every 10th time
    if (this._averageHeightCounter < 10 || !(this._averageHeightCounter % 10)) {
      /// average height of first 50 items
      const averageItems: any[] = Object.values(this._measures).filter((a: any) => a.height).slice(0, 50);
      const averageTotal: number = averageItems.reduce((total: number, a: any) => (total + a.height), 0);

      this._averageHeight = Math.round(averageTotal / averageItems.length);

      // log('updateAverageHeight:',
      //   this._averageHeightCounter, this._wrapperHeight,
      //   this._bufferMax * this._averageHeight
      // );

      /// if height is more than buffered items height, double bufferCount
      if (this._wrapperHeight > (this._bufferMax * this._averageHeight)) {
        this._bufferCount = this._bufferCount * 2;
        this._bufferCount2 = this._bufferCount * 2;
        this._bufferMax = this._bufferCount * 5;
      }
    }

    this._averageHeightCounter += 1;
  }

  private storeState(): void {
    if (this._destroyed) { return; }

    if (this.items.length && this._averageHeight) {
      /// find next visible item
      const { scrollTop } = this;

      let item = this._viewPortItems.find(a => {
        const { [this._idProp]: _id } = a;
        const _measureItem = this._measures[_id];
        if (_measureItem && (_measureItem.top >= scrollTop)) {
          return true;
        }
      });

      item = item ? item : this._viewPortItems[this._viewPortItems.length - 1];

      const { [this._idProp]: id } = item;
      const measureItem = this._measures[id];
      this._currentId = id;
      this._currentOffset = measureItem.top - scrollTop;

      // log('storeState:', {
      //   _currentId: this._currentId,
      //   _currentOffset: this._currentOffset,
      //   scrollTop,
      //   measureItem
      // });
      return;
    }

    this._currentId = null;
    this._currentOffset = 0;
  }

  private restoreState(times: number = 3): void {
    if (this._destroyed) { return; }

    this.nextFrame(() => {
      /// either _currentId or _scrollToId
      const toId: number | string =
        isNumber(this._currentId) ? this._currentId
        : (isNumber(this._scrollToId) ? this._scrollToId : null);

      if (toId) {
        const { scrollTop } = this;
        const measureItem = this._measures[toId];

        let itemTop: number = measureItem ? measureItem.top : 0;

        if (!measureItem) {
          const currentItem = this.items.find(a => (a[this._idProp] === toId));
          if (currentItem) {
            const itemIndex: number = this.items.indexOf(currentItem);
            itemTop = itemIndex * this._averageHeight;
          } else {
            this._currentId = null;
            this._scrollToId = null;
            this._currentOffset = 0;
            /// _currentId item not found in items
            console.error(`ng-vscroll-box: Item not found with ${this._idProp} as ${toId}`);
            return;
            /// dont throw, suppose toId item was deleted just now
            // throw new Error();
          }
        }

        let newScrollTop: number = itemTop - this._currentOffset;

        /// maximium scrollTop
        const topMax: number = this._scrollHeight - this._wrapperHeight;

        newScrollTop = Math.min(topMax, newScrollTop);

        const isItDiff: boolean = isDiff(scrollTop, newScrollTop);

        if (isItDiff) {
          this.wrapperElement.scrollTo({ top: newScrollTop, left: 0 });
          this.scrollTop = newScrollTop;
          this.doCheck('restoreState');
        }

        const again: boolean = (isItDiff || !measureItem || !measureItem.height);
        const newTimes: number = again ? times : (times - 1);

        // log('restoreState:', {
        //   newTimes,
        //   isItDiff,
        //   _currentId: this._currentId,
        //   _scrollToId: this._scrollToId,
        //   scrollTop,
        //   newScrollTop,
        //   start: this._startIndex,
        //   end: this._endIndex,
        //   again,
        //   itemH: measureItem.height
        // });

        if (newTimes) {
          this.timeOut(() => {
            this.restoreState(newTimes);
          });
        } else {
          this._currentId = null;
          this._scrollToId = null;
          this._currentOffset = 0;
        }
      }
    });
  }

  /// internal scrollToPosition
  private toPosition(top: number, toBottom: boolean = false, times: number = 3): void {
    if (this._destroyed) { return; }

    this.nextFrame(() => {
      const { scrollTop } = this;
      let newScrollTop: number = top;

      /// items has less or no height
      if (this._scrollHeight < this._wrapperHeight) {
        return;
      }

      /// maximium scrollTop
      const topMax: number = this._scrollHeight - this._wrapperHeight;

      if (toBottom || (newScrollTop > topMax)) {
        newScrollTop = topMax;
        if (times < 2) {
          newScrollTop = this._scrollTopMax;
        }
      }

      const isItDiff: boolean = isDiff(scrollTop, newScrollTop);

      if (isItDiff) {
        this.wrapperElement.scrollTo({ top: newScrollTop + 10, left: 0 });
        this.scrollTop = newScrollTop;
        this.doCheck('toPosition');
      }

      const again: boolean = (isItDiff);
      const newTimes: number = again ? times : (times - 1);

      if (newTimes) {
        this.timeOut(() => {
          this.toPosition(top, toBottom, newTimes);
        });
      }

      // log('toPosition:', {
      //   newTimes,
      //   top, toBottom,
      //   _scrollTop: this._scrollTop,
      //   _scrollHeight: this._scrollHeight,
      //   _wrapperHeight: this._wrapperHeight,
      //   newScrollTop,
      //   isItDiff, again,
      // });
    });
  }

  private emitChange(newStartIndex: number): void {
    this.nextFrame(() => {

      let startIndex = newStartIndex;

      /// startIndex, endIndex
      const { length } = this.items;
      const endIndex = Math.min(length, startIndex + this._bufferMax);
      startIndex = Math.max(0, endIndex - this._bufferMax);

      const viewPortItems: any[] = this.items.slice(startIndex, endIndex);

      /// if same
      // TODO shallow compare each viewPortItem
      // if ((startIndex === this._startIndex)
      //   && (endIndex === this._endIndex)
      //   && (viewPortItems === this.viewPortItems)) {
      //   return;
      // }

      /// update internal
      this._startIndex = startIndex;
      // this._endIndex = endIndex;
      this._viewPortItems = viewPortItems;

      const changeEvent: ChangeEvent = {
        viewPortItems,
        startIndex,
        endIndex,
      };

      // log('emitChange', { startIndex, endIndex });
      this.changeEvent.emit(changeEvent);
    });
  }

  private emitUpdate(from: string = ''): void {
    this.nextFrame(() => {

      const { scrollTop, _scrollHeight, _wrapperHeight } = this;
      const atTop: boolean = (scrollTop === 0);
      const atBottom: boolean = ((_scrollHeight - scrollTop) <= _wrapperHeight);

      const updateEvent: UpdateEvent = {
        scrollTop,
        scrollHeight: _scrollHeight,
        height: _wrapperHeight,
        atTop,
        atBottom,
      };

      // log('emitUpdate:', from, updateEvent);
      this.updateEvent.emit(updateEvent);
    });
  }

}
